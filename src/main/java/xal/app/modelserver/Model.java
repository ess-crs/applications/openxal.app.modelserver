/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.modelserver;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import xal.ca.ChannelFactory;
import xal.extension.jels.smf.impl.FieldMapFactory;
import xal.extension.jels.smf.impl.RfFieldMap1D;
import xal.model.ModelException;
import xal.model.alg.EnvelopeTracker;
import xal.model.probe.Probe;
import xal.sim.scenario.AlgorithmFactory;
import xal.sim.scenario.ProbeFactory;
import xal.sim.scenario.Scenario;
import xal.sim.sync.SynchronizationException;
import xal.smf.Accelerator;
import xal.smf.AcceleratorSeqCombo;
import xal.smf.ApertureProfile;
import xal.smf.data.XMLDataManager;
import xal.tools.apputils.Preferences;

/**
 * This class runs the model and updates the server with simulation results.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class Model {

    private String MODEL_STRING = "XAL-MDL:";

    private Scenario model;
    private SimResultsServer simServer;
    private InstrumentationServer instrumentationServer;

    private Calendar calendar = Calendar.getInstance();

    /**
     * The Model class is used to run the simulations, and to create and update
     * EPICS servers.
     *
     * @param mode Running mode. It should be either "DESIGN" or "LIVE".
     * @param CHANNEL_SERVER_FACTORY The Channel Factory object to use.
     * @throws Exception Throws this exception is the model cannot be
     * instantiated or run.
     */
    public Model(String mode, ChannelFactory CHANNEL_SERVER_FACTORY) throws Exception {
        if (!"DESIGN".equals(mode) && !"LIVE".equals(mode)) {
            throw (new Exception("Mode should be either \"DESIGN\" or \"LIVE\"."));
        }
        
        java.util.prefs.Preferences defaults = Preferences.nodeForPackage(ModelServer.class);
        MODEL_STRING = defaults.get("modelPrefix", MODEL_STRING);
        
        String prefix = MODEL_STRING + mode + ":";

        simServer = new SimResultsServer(prefix, CHANNEL_SERVER_FACTORY);

        Accelerator accelerator = XMLDataManager.loadDefaultAccelerator();

        AcceleratorSeqCombo sequence = new AcceleratorSeqCombo("Full Accelerator", accelerator.getSequences());

        // Reducing the number of points for RF Fieldmaps
        FieldMapFactory.getInstances().values().stream().filter(
                (fieldMap) -> (fieldMap instanceof RfFieldMap1D)).forEachOrdered((fieldMap) -> {
                    ((RfFieldMap1D) fieldMap).setNumberOfPoints(200);
                });

        ApertureProfile aperture = sequence.getAperProfile();

        simServer.updateApertureValue(aperture);

        try {
            EnvelopeTracker envelopeTracker = AlgorithmFactory.createEnvelopeTracker(sequence);

            Probe probe = ProbeFactory.getEnvelopeProbe(sequence, envelopeTracker);

            model = Scenario.newScenarioFor(sequence);

            model.setProbe(probe);

            model.setSynchronizationMode(mode);

        } catch (InstantiationException | ModelException ex) {
            throw (ex);
        }

        // Create channels for all the instrumentation.
        instrumentationServer = new InstrumentationServer(prefix, sequence, CHANNEL_SERVER_FACTORY);
    }

    /**
     * Run the model again, syncing with live data if needed, and update PVs
     * with results.
     *
     * If an exception occurs, the status is changed to "DISCONNECTED" and PVs
     * to 0
     */
    public void run() {
        model.resetProbe();
        try {
            model.resync();
        } catch (SynchronizationException ex) {
            Logger.getLogger(ModelServer.class.getName()).log(Level.INFO, "Some PVs could not be reached.");
            simServer.reset();
            instrumentationServer.reset();
            simServer.updateStatus(calendar.getTime(), "DISCONNECTED");
            return;
        }

        Date timestamp = calendar.getTime();

        try {
            model.run();
        } catch (ModelException ex) {
            Logger.getLogger(ModelServer.class.getName()).log(Level.WARNING, "Exception while running the model.", ex);
            simServer.reset();
            instrumentationServer.reset();
            simServer.updateStatus(timestamp, "SIMULATION ERROR");
        }
        Probe probe = model.getProbe();

        simServer.updateProbeValues(probe);
        simServer.updateStatus(timestamp, "RUNNING");
        instrumentationServer.updateResults(probe);
    }
}
