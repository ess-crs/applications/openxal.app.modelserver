/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.modelserver;

import eu.ess.xaos.tools.annotation.ServiceLoaderUtilities;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;
import xal.ca.ChannelFactory;
import xal.model.probe.Probe;
import xal.smf.AcceleratorSeq;

/**
 * Creates channels for the instrumentation devices.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class InstrumentationServer {

    private List<VirtualInstrument> instruments = new ArrayList();

    /**
     * Instantiates a new InstrumentationServer for the given sequence.
     *
     * @param prefix Prefix to be added to all EPICS signals.
     * @param sequence Accelerator sequence to scan to find diagnostics.
     * @param CHANNEL_SERVER_FACTORY Channel factory to be used.
     */
    public InstrumentationServer(String prefix, AcceleratorSeq sequence, ChannelFactory CHANNEL_SERVER_FACTORY) {
        List<VirtualInstrument> instrumentClasses = ServiceLoaderUtilities.of(ServiceLoader.load(VirtualInstrument.class));

        for (VirtualInstrument instrumentClass : instrumentClasses) {
            sequence.getAllNodesOfType(instrumentClass.getType()).forEach((node) -> {
                VirtualInstrument instrument = instrumentClass.newInstance();
                instrument.initialize(node, prefix, CHANNEL_SERVER_FACTORY);
                instruments.add(instrument);
            });
        }
    }

    /**
     * Update the EPICS signal values for all the instruments.
     *
     * @param probe Probe that contains the latest simulation results.
     */
    public void updateResults(Probe probe) {
        instruments.forEach((instrument) -> {
            instrument.updateResults(probe);
        });
    }

    /**
     * Reset all EPICS signals for all the instruments.
     */
    public void reset() {
        instruments.forEach((instrument) -> {
            instrument.reset();
        });
    }

}
