/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.modelserver;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import xal.ca.Channel;
import xal.ca.ChannelFactory;
import xal.ca.PutException;
import xal.model.probe.Probe;
import xal.model.probe.traj.EnvelopeProbeState;
import xal.model.probe.traj.ProbeState;
import xal.model.probe.traj.Trajectory;
import xal.smf.ApertureProfile;
import xal.tools.beam.CovarianceMatrix;

/**
 * Create channels for the simulation results.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class SimResultsServer {

    private ChannelFactory CHANNEL_SERVER_FACTORY;

    // PV names
    private static final String STATUS = "STATUS";
    private static final String TIMESTAMP = "TIMESTAMP";

    private static final String POSITION = "POSITION_S";

    private static final String MUX = "MEAN_X";
    private static final String MUY = "MEAN_Y";
    private static final String MUZ = "MEAN_Z";

    private static final String SIGMAX = "SIGMA_X";
    private static final String SIGMAY = "SIGMA_Y";
    private static final String SIGMAZ = "SIGMA_Z";

    private static final String ENERGY = "KINETIC_ENERGY";

    private static final String POSITION_APERTURE = "POSITION_APER";
    private static final String APERTURE_X = "APERTURE_X";
    private static final String APERTURE_Y = "APERTURE_Y";

    // Values
    private double[] positionValue;

    private double[] muXValue;
    private double[] muYValue;
    private double[] muZValue;

    private double[] sigmaXValue;
    private double[] sigmaYValue;
    private double[] sigmaZValue;

    private double[] energyValue;

    // Channels
    private Channel status = null;
    private Channel timestamp = null;

    private Channel position = null;

    private Channel muX = null;
    private Channel muY = null;
    private Channel muZ = null;

    private Channel sigmaX = null;
    private Channel sigmaY = null;
    private Channel sigmaZ = null;

    private Channel energy = null;

    private Channel positionAperture = null;
    private Channel apertureX = null;
    private Channel apertureY = null;

    /**
     * This constructor creates all the PVs.
     *
     * @param prefix Prefix of the channel name
     * @param CHANNEL_SERVER_FACTORY The channel factory object.
     */
    public SimResultsServer(String prefix, ChannelFactory CHANNEL_SERVER_FACTORY) {
        this.CHANNEL_SERVER_FACTORY = CHANNEL_SERVER_FACTORY;

        // Create PVs
        status = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + STATUS);
        timestamp = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + TIMESTAMP);

        position = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + POSITION);

        muX = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + MUX);
        muY = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + MUY);
        muZ = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + MUZ);

        sigmaX = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + SIGMAX);
        sigmaY = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + SIGMAY);
        sigmaZ = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + SIGMAZ);

        energy = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + ENERGY);

        apertureX = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + APERTURE_X);
        apertureY = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + APERTURE_Y);
        positionAperture = this.CHANNEL_SERVER_FACTORY.getChannel(prefix + POSITION_APERTURE);
    }

    /**
     * Updating the aperture PVs.
     *
     * @param apertureProfile Aperture profile to be used.
     */
    public void updateApertureValue(ApertureProfile apertureProfile) {
        try {
            positionAperture.putVal(apertureProfile.getProfileXArray()[0]);
            apertureX.putVal(apertureProfile.getProfileXArray()[1]);
            apertureY.putVal(apertureProfile.getProfileYArray()[1]);
        } catch (PutException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Updating PVs related to beam parameters.
     *
     * @param probe Probe object with the new parameters.
     */
    public void updateProbeValues(Probe probe) {
        Trajectory<? extends ProbeState<?>> trajectory = probe.getTrajectory();
        int numStates = trajectory.numStates();
        List<? extends ProbeState<?>> stateElement = trajectory.getStatesViaIndexer();

        positionValue = new double[numStates];
        muXValue = new double[numStates];
        muYValue = new double[numStates];
        muZValue = new double[numStates];
        sigmaXValue = new double[numStates];
        sigmaYValue = new double[numStates];
        sigmaZValue = new double[numStates];
        energyValue = new double[numStates];

        for (int i = 0; i < numStates; i++) {

            ProbeState<?> pState = stateElement.get(i);

            if (pState instanceof EnvelopeProbeState) {

                EnvelopeProbeState state = (EnvelopeProbeState) pState;
                CovarianceMatrix matrix = state.getCovarianceMatrix();

                positionValue[i] = state.getPosition();

                muXValue[i] = matrix.getMeanX();
                muYValue[i] = matrix.getMeanY();
                muZValue[i] = matrix.getMeanZ();

                sigmaXValue[i] = matrix.getSigmaX();
                sigmaYValue[i] = matrix.getSigmaY();
                sigmaZValue[i] = matrix.getSigmaZ();

                energyValue[i] = state.getKineticEnergy();
            }

        }

        try {
            position.putVal(positionValue);

            muX.putVal(muXValue);
            muY.putVal(muYValue);
            muZ.putVal(muZValue);

            sigmaX.putVal(sigmaXValue);
            sigmaY.putVal(sigmaYValue);
            sigmaZ.putVal(sigmaZValue);

            energy.putVal(energyValue);
        } catch (PutException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method to be called when some PVs are disconnected.
     */
    public void reset() {
        try {
            position.putVal(0.0);

            muX.putVal(0.0);
            muY.putVal(0.0);
            muZ.putVal(0.0);

            sigmaX.putVal(0.0);
            sigmaY.putVal(0.0);
            sigmaZ.putVal(0.0);

            energy.putVal(0.0);

            positionAperture.putVal(0.0);
            apertureX.putVal(0.0);
            apertureY.putVal(0.0);
        } catch (PutException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Updating status and timestamp PVs after a simulation is finished.
     *
     * @param timestamp Timestamp when the simulation is done.
     * @param status Status of the simulation.
     */
    public void updateStatus(Date timestamp, String status) {
        try {
            this.status.putVal(status);
            SimpleDateFormat dateOnly = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            this.timestamp.putVal(dateOnly.format(timestamp));
        } catch (PutException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
}
