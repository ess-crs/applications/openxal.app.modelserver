/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.modelserver;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import xal.ca.ChannelFactory;
import xal.tools.apputils.Preferences;

/**
 * Model Server is an Open XAL based application that runs an online model of
 * the accelerator, using both the design parameters and the live machine
 * parameters, and published the results as PVs.
 *
 * The results include beam kinetic energy, centroid, and envelope; aperture;
 * and estimations of measurements for some beam diagnostics.
 *
 * It is intended for control room OPIs and other applications that require
 * simple simulations.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public class ModelServer {

    private static ChannelFactory CHANNEL_SERVER_FACTORY = ChannelFactory.newServerFactory();

    /**
     * Entry point of the server.
     *
     * @param args the command line arguments. Only 1 optional argument, the
     * suffix for the PVs if a Virtual Machine is used. If not defined, it will
     * try to connect to the real machine.
     */
    public static void main(String[] args) {
        // Setting client channel factory to test mode to use data from virtual machine.
        // First tries to get suffix from Java Preferences
        java.util.prefs.Preferences defaults = Preferences.nodeForPackage(ModelServer.class);
        String testSuffix = defaults.get("testSuffix", "");
        if (!testSuffix.equals("")) {
            ChannelFactory.defaultFactory().setTest(true);
            ChannelFactory.defaultFactory().setTestSuffix(testSuffix);
        }
        // Overwrite suffix with command line argument, if exists.
        if (args.length == 1) {
            testSuffix = args[0];
            ChannelFactory.defaultFactory().setTest(true);
            ChannelFactory.defaultFactory().setTestSuffix(testSuffix);
        }

        // Design model runs only once at the beginning. Restart is required if the model
        // is modified (also for live).
        Model designModel = null;
        try {
            designModel = new Model("DESIGN", CHANNEL_SERVER_FACTORY);
        } catch (Exception ex) {
            Logger.getLogger(ModelServer.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (designModel != null) {
            designModel.run();
        }

        // Live model runs continuously with a delay of 1 second between runs.
        Model liveModel = null;
        try {
            liveModel = new Model("LIVE", CHANNEL_SERVER_FACTORY);
        } catch (Exception ex) {
            Logger.getLogger(ModelServer.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (liveModel != null) {
            final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
            try {
                executorService.scheduleWithFixedDelay(liveModel::run, 0, 1, TimeUnit.SECONDS).get();
            } catch (InterruptedException | ExecutionException ex) {
                Logger.getLogger(ModelServer.class.getName()).log(Level.SEVERE, null, ex);
                executorService.shutdownNow();
            }
        }

        // Disposing the EPICS channels before exiting.
        if (CHANNEL_SERVER_FACTORY != null) {
            ChannelFactory.disposeAll();
            CHANNEL_SERVER_FACTORY = null;
        }
    }
}
