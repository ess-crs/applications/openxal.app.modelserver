/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.modelserver.impl;

import eu.ess.xaos.tools.annotation.ServiceProvider;
import java.util.logging.Level;
import java.util.logging.Logger;
import xal.app.modelserver.InstrumentationServer;
import xal.app.modelserver.VirtualInstrument;
import xal.ca.Channel;
import xal.ca.ChannelFactory;
import xal.ca.PutException;
import xal.model.probe.EnvelopeProbe;
import xal.model.probe.Probe;
import xal.model.probe.traj.EnvelopeProbeState;
import xal.smf.AcceleratorNode;
import xal.smf.impl.BPM;
import xal.tools.beam.CovarianceMatrix;

/**
 * Creates the channels corresponding to a BPM assign values using simulation results from a Probe object.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
@ServiceProvider(service = VirtualInstrument.class, order = 1)
public class VirtualBPM implements VirtualInstrument {

    private BPM node;
    private Channel xAvgC;
    private Channel yAvgC;
    private Channel ampAvgC;
    private Channel phaseAvgC;
    private Channel xTBTC;
    private Channel yTBTC;
    private Channel ampTBTC;
    private Channel phaseTBTC;
    private Channel tAvgLenC;

    // Channels to update the Virtual Machine PVs if it is running.
    private Channel xAvgCVM;
    private Channel yAvgCVM;

    @Override
    public VirtualInstrument newInstance() {
        return new VirtualBPM();
    }

    @Override
    public String getType() {
        return BPM.TYPE;
    }

    @Override
    public void initialize(AcceleratorNode bpmNode, String prefix, ChannelFactory CHANNEL_SERVER_FACTORY) {
        node = (BPM) bpmNode;
        String signalxAvgC = bpmNode.channelSuite().getSignal(BPM.X_AVG_HANDLE);
        xAvgC = CHANNEL_SERVER_FACTORY.getChannel(prefix + signalxAvgC);
        String signalyAvgC = bpmNode.channelSuite().getSignal(BPM.Y_AVG_HANDLE);
        yAvgC = CHANNEL_SERVER_FACTORY.getChannel(prefix + signalyAvgC);
        String signalampAvgC = bpmNode.channelSuite().getSignal(BPM.AMP_AVG_HANDLE);
        ampAvgC = CHANNEL_SERVER_FACTORY.getChannel(prefix + signalampAvgC);
        String signalphaseAvgC = bpmNode.channelSuite().getSignal(BPM.PHASE_AVG_HANDLE);
        phaseAvgC = CHANNEL_SERVER_FACTORY.getChannel(prefix + signalphaseAvgC);
        String signalxTBTC = bpmNode.channelSuite().getSignal(BPM.X_TBT_HANDLE);
        xTBTC = CHANNEL_SERVER_FACTORY.getChannel(prefix + signalxTBTC);
        String signalyTBTC = bpmNode.channelSuite().getSignal(BPM.Y_TBT_HANDLE);
        yTBTC = CHANNEL_SERVER_FACTORY.getChannel(prefix + signalyTBTC);
        String signalampTBTC = bpmNode.channelSuite().getSignal(BPM.AMP_TBT_HANDLE);
        ampTBTC = CHANNEL_SERVER_FACTORY.getChannel(prefix + signalampTBTC);
        String signalphaseTBTC = bpmNode.channelSuite().getSignal(BPM.PHASE_TBT_HANDLE);
        phaseTBTC = CHANNEL_SERVER_FACTORY.getChannel(prefix + signalphaseTBTC);
        String signaltAvgLenC = bpmNode.channelSuite().getSignal(BPM.T_AVG_LEN_HANDLE);
        tAvgLenC = CHANNEL_SERVER_FACTORY.getChannel(prefix + signaltAvgLenC);

        /* Connect to Virtual Machine to update instrumentation readings.
           If model is only run in Design mode, ensure that the default factory
           is NOT set to Test mode */
        if (ChannelFactory.defaultFactory().isTest()) {
            xAvgCVM = ChannelFactory.defaultFactory().getChannel(signalxAvgC);
            yAvgCVM = ChannelFactory.defaultFactory().getChannel(signalyAvgC);
        }
    }

    @Override
    public void updateResults(Probe probe) {
        if (probe instanceof EnvelopeProbe) {
            EnvelopeProbeState stateAtBPM = (EnvelopeProbeState) probe.getTrajectory().stateForElement(node.getId());

            CovarianceMatrix covarianceMatrix = stateAtBPM.getCovarianceMatrix();

            try {
                xAvgC.putVal(covarianceMatrix.getMeanX());
                yAvgC.putVal(covarianceMatrix.getMeanY());

                if (ChannelFactory.defaultFactory().isTest()) {
                    xAvgCVM.putVal(covarianceMatrix.getMeanX());
                    yAvgCVM.putVal(covarianceMatrix.getMeanY());
                }
            } catch (PutException ex) {
                Logger.getLogger(InstrumentationServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Logger.getLogger(getClass().getName(), "BPM requires an EnvelopeProbe object.");
        }
    }

    @Override
    public void reset() {
        try {
            xAvgC.putVal(0.0);
            yAvgC.putVal(0.0);
            ampAvgC.putVal(0.0);
            phaseAvgC.putVal(0.0);
            xTBTC.putVal(0.0);
            yTBTC.putVal(0.0);
            ampTBTC.putVal(0.0);
            phaseTBTC.putVal(0.0);
            tAvgLenC.putVal(0.0);
        } catch (PutException ex) {
            Logger.getLogger(InstrumentationServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
