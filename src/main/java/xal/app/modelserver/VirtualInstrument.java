/*
 * Copyright (C) 2020 European Spallation Source ERIC.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package xal.app.modelserver;

import xal.smf.AcceleratorNode;
import xal.ca.ChannelFactory;
import xal.model.probe.Probe;

/**
 * Interface for virtual instruments that get simulation results from a Probe
 * object and update the PVs accordingly.
 *
 * The implementations must have the annotation
 * <code>@ServiceProvider(service = VirtualInstrument.class, order = 1)</code>
 * to be found and used by the virtual machine.
 *
 * @author Juan F. Esteban Müller <JuanF.EstebanMuller@ess.eu>
 */
public interface VirtualInstrument {

    /**
     * Creates a new instance of the class.
     *
     * @return The instance.
     */
    public VirtualInstrument newInstance();

    /**
     * To obtain the instrument type.
     *
     * @return The device s_strType field.
     */
    public String getType();

    /**
     * The initializer creates the channels for each handle.
     *
     * @param node Accelerator Node object corresponding to the instrument.
     * @param prefix A prefix added to the channel name to distinguish it from
     * the real PV.
     * @param CHANNEL_SERVER_FACTORY The Channel Factory.
     */
    public void initialize(AcceleratorNode node, String prefix, ChannelFactory CHANNEL_SERVER_FACTORY);

    /**
     * This method updates the results after a new simulation is run.
     *
     * @param probe Probe object with the latest simulation status.
     */
    public void updateResults(Probe probe);

    /**
     * Resets the values of all PVs to a defined state when the virtual machine
     * is not running.
     */
    public void reset();
}
